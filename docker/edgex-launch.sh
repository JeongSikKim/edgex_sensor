#!/bin/sh

COMPOSE_FILE=./docker-compose.yml

echo "Pulling latest compose file..."
docker-compose -f $COMPOSE_FILE pull

echo "Starting volume"
docker-compose -f $COMPOSE_FILE up -d volume

echo "Starting config-seed"
docker-compose -f $COMPOSE_FILE up -d config-seed
echo "Starting mongo"
docker-compose -f $COMPOSE_FILE up -d mongo

echo "Sleeping before launching remaining services"
sleep 15

echo "Starting support-logging"
docker-compose -f $COMPOSE_FILE up -d logging
echo "Starting core-metadata"
docker-compose -f $COMPOSE_FILE up -d metadata
echo "Starting core-data"
docker-compose -f $COMPOSE_FILE up -d data
echo "Starting core-command"
docker-compose -f $COMPOSE_FILE up -d command
echo "Starting scheduler"
docker-compose -f $COMPOSE_FILE up -d scheduler
echo "Starting core-export-client"
docker-compose -f $COMPOSE_FILE up -d export-client
echo "Starting core-export-distro"
docker-compose -f $COMPOSE_FILE up -d export-distro
echo "Starting support-notifications"
docker-compose -f $COMPOSE_FILE up -d notifications
echo "Starting rulesengine"
docker-compose -f $COMPOSE_FILE up -d rulesengine
echo "Starting device-virtual"
docker-compose -f $COMPOSE_FILE up -d device-virtual
echo "Starting edgex-web-ui"
docker-compose -f $COMPOSE_FILE up -d edgex-web-ui