import EdgeXRestAPI
import EdgeXDictionary

servicename = 'iec-device-hecas-sensor-service'
addressablename = servicename + '-addressable'
deviceservicename = 'hecas-sensor-deviceservice'
profilename = 'hecas.sensor.profile'
DeviceName = 'iec-device-hecas-sensor-device'


def initRegistrationInformations(edgex_host='http://localhost:48081/api/v1/',
                                 edgex_coredata_host='http://localhost:48080/api/v1/'):
    Service_Info_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST=edgex_host)
    Service_Value_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST=edgex_coredata_host)

    # POST Addressable
    sensor_addressable = EdgeXDictionary.Addressable(name=addressablename, protocol='HTTP',
                                                     method='POST', address='127.0.0.1', port=49002,
                                                     path='/hecas_sensor',
                                                     publisher='none', user='none', password='none', topic='none')

    Service_Info_restHelper.post_addressable(sensor_addressable)

    # # POST Device Service
    sensor_deviceservice = EdgeXDictionary.DeviceService(name=deviceservicename,
                                                         description='hecas sensor device service',
                                                         labels=["noise", "value"], adminState='unlocked',
                                                         operatingState='enabled',
                                                         addressable={"name": addressablename})

    Service_Info_restHelper.post_deviceService(sensor_deviceservice)

    # POST Device Profile
    switch_deviceProfile = EdgeXDictionary.DeviceProfile(name=profilename, manufacturer='Querensys', model='sensor',
                                                         description='device-sensor-profile',
                                                         labels=['noise', 'gas', 'vibration'],
                                                         commands=[{"name": "noise",
                                                                    "get": {"path": "/api/v1/devices/{deviceId}/noise",
                                                                            "response": [{"code": "200",
                                                                                          "description": "noise",
                                                                                          "expectedValues": [
                                                                                              "noise"]},
                                                                                         {"code": "503",
                                                                                          "description": "noise",
                                                                                          "expectedValues": [
                                                                                              "service unavailable"]}
                                                                                         ],
                                                                            "path": "/api/v1/devices/{deviceId}/gas",
                                                                            "response": [{"code": "200",
                                                                                          "description": "gas",
                                                                                          "expectedValues": [
                                                                                              "gas"]},
                                                                                         {"code": "503",
                                                                                          "description": "gas",
                                                                                          "expectedValues": [
                                                                                              "service unavailable"]}
                                                                                         ],
                                                                            "path": "/api/v1/devices/{deviceId}/vibration",
                                                                            "response": [{"code": "200",
                                                                                          "description": "vib",
                                                                                          "expectedValues": [
                                                                                              "vib"]},
                                                                                         {"code": "503",
                                                                                          "description": "vib",
                                                                                          "expectedValues": [
                                                                                              "service unavailable"]}
                                                                                         ]
                                                                            }
                                                                    }])

    Service_Info_restHelper.post_deviceProfile(switch_deviceProfile)

    # POST Device Information
    sensor_deviceInfo = EdgeXDictionary.DeviceInfo(name=DeviceName,
                                                   description='hecas sensor device', adminState='UNLOCKED',
                                                   operatingState='ENABLED', addressable={'name': addressablename},
                                                   labels=['noise', 'value'], location='',
                                                   service={'name': deviceservicename},
                                                   profile={'name': profilename})

    Service_Info_restHelper.post_deviceInfo(sensor_deviceInfo)

    # POST Device ValueDescriptor Information
    sensor_valueDescriptor_noise = EdgeXDictionary.ValueDescriptor(name='noise',
                                                                   min='0',
                                                                   max='10000',
                                                                   type='F',
                                                                   uomLabel='decibel',
                                                                   defaultValue='0',
                                                                   formatting='%s',
                                                                   labels=['noise'])
    sensor_valueDescriptor_gas = EdgeXDictionary.ValueDescriptor(name='gas',
                                                                 min='0',
                                                                 max='20000',
                                                                 type='F',
                                                                 uomLabel='ppm',
                                                                 defaultValue='0',
                                                                 formatting='%s',
                                                                 labels=['gas'])
    sensor_valueDescriptor_vibration = EdgeXDictionary.ValueDescriptor(name='vibration',
                                                                       min='0',
                                                                       max='1000',
                                                                       type='F',
                                                                       uomLabel='mm',
                                                                       defaultValue='0',
                                                                       formatting='%s',
                                                                       labels=['vib'])

    Service_Value_restHelper.post_valueDescriptor(sensor_valueDescriptor_noise)
    Service_Value_restHelper.post_valueDescriptor(sensor_valueDescriptor_gas)
    Service_Value_restHelper.post_valueDescriptor(sensor_valueDescriptor_vibration)


if __name__ == "__main__":
    initRegistrationInformations()
