# -*- coding: utf-8 -*-
import json
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import requests
import time

sensor_generator = {"sensor": "sensor_emulator",
                    "stats":[
                        {"vib": {"vibration_sensor_name": "vb-8206sd",
                                 "vibration": ''
                                 },
                         "gas": {"gas_sensor_name": "center-321",
                                 "gas_fw_ver": "1.0.0",
                                 "carbon_monoxide": 0.0,
                                 "nitrogen_dioxide": 0.0,
                                 "ammonia": 0.0,
                                 "propane": 0.0,
                                 "iso_butane": 0.0,
                                 "methane": 0.0,
                                 "hydrogen": 0.0,
                                 "ethanol": 0.0,
                                 "voc_sensor_name": "YVOCMK02-BDB49",
                                 "voc": 450.0
                                 },
                         "sd": {
                             "sound_name": "center-321",
                             "gas_sound_fw_ver": "1.0.0",
                             "sound": 74.56
                         }
                         }]

                    }

print(type(sensor_generator))
# print(sensor_generator['stats']['sd']['sound'])

# json_val = json.dumps(sensor_generator, sort_keys=True, indent = 4)
# print type(json_val)
# print (json_val)

str_data = json.dumps(sensor_generator)
# print(str_data)
print(type(str_data))


resp = json.loads(str_data)
# print(resp)
print(type(resp))
#
# print(resp['stats']['sd'])
# print(resp['stats']['sd']['sound'])


# def requests_retry_session(
#         retries=3,
#         backoff_factor=0.3,
#         status_forcelist=(500, 502, 504),
#         session=None,
# ):
#     session = session or requests.Session()
#     retry = Retry(
#         total=retries,
#         read=retries,
#         connect=retries,
#         backoff_factor=backoff_factor,
#         status_forcelist=status_forcelist,
#     )
#     adapter = HTTPAdapter(max_retries=retry)
#     session.mount('http://', adapter)
#     session.mount('https://', adapter)
#     return session
#
#
# t0 = time.time()
# try:
#     response = requests_retry_session().put(
#         'http://localhost:49002/api/v1/devices/{deviceId}/noise', data=str_data,
#         timeout=5
#     )
# except Exception as x:
#     print('It failed :(', x.__class__.__name__)
# else:
#     print('It eventually worked', response.status_code)
# finally:
#     t1 = time.time()
#     print('Took', t1 - t0, 'seconds')

while True:
    requests.put("http://localhost:49002/api/v1/data", str_data)
    print(str_data)
# requests.put("http://localhost:49002/api/v1/data", resp)
# print(resp)
    time.sleep(1)
