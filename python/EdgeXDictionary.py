class Addressable:
    def __init__(self, name, protocol, method, address, port, path, publisher, user, password, topic):
        self.name = name
        self.protocol = protocol
        self.address = address
        self.method = method
        self.port = port
        self.path = path
        self.publisher = publisher
        self.user = user
        self.password = password
        self.topic = topic

    def getData(self):
        return {
            'name': self.name,
            'protocol': self.protocol,
            'address': self.address,
            'method': self.method,
            'port': self.port,
            'path': self.path,
            'publisher': self.publisher,
            'user': self.user,
            'password': self.password,
            'topic': self.topic
        }


class DeviceService:
    def __init__(self, name, description, labels, adminState, operatingState, addressable):
        self.name = name
        self.description = description
        self.labels = labels
        self.adminState = adminState
        self.operatingState = operatingState
        self.addressable = addressable

    def getData(self):
        return {
            'name': self.name,
            'description': self.description,
            "labels": self.labels,
            "adminState": self.adminState,
            "operatingState": self.operatingState,
            "addressable": self.addressable
        }


class DeviceProfile:
    def __init__(self, name, manufacturer, description, model, labels, commands):
        self.name = name
        self.description = description
        self.manufacturer = manufacturer
        self.model = model
        self.labels = labels
        self.commands = commands

    def getData(self):
        return {
            'name': self.name,
            'description': self.description,
            'manufacturer': self.manufacturer,
            'model': self.model,
            "labels": self.labels,
            "commands": self.commands
        }


class DeviceInfo:
    def __init__(self, name, description,
                 adminState, operatingState,
                 addressable, labels, location, service, profile):
        self.name = name
        self.description = description
        self.adminState = adminState
        self.operatingState = operatingState
        self.labels = labels
        self.addressable = addressable
        self.location = location
        self.service = service
        self.profile = profile

    def getData(self):
        return {
            'name': self.name,
            'description': self.description,
            'adminState': self.adminState,
            'operatingState': self.operatingState,
            "labels": self.labels,
            "addressable": self.addressable,
            "service": self.service,
            "profile": self.profile
        }


class ValueDescriptor(object):
    def __init__(self, name,
                 min, max, type,
                 uomLabel, defaultValue, formatting,
                 labels):
        self.name = name
        self.min = min
        self.max = max
        self.type = type
        self.labels = labels
        self.uomLabel = uomLabel
        self.defaultValue = defaultValue
        self.formatting = formatting

    def getData(self):
        return {
            'name': self.name,
            'min': self.min,
            'max': self.max,
            "labels": self.labels,
            "type": self.type,
            "uomLabel": self.uomLabel,
            "defaultValue": self.defaultValue,
            "formatting": self.formatting
        }


class NoiseValue(object):
    def __init__(self, device,
                 readings):
        self.device = device
        self.readings = readings

    def getData(self):
        return {
            'device': self.device,
            'reading': self.readings
        }

class GasValue(object):
    def __init__(self, device,
                 readings):
        self.device = device
        self.readings = readings

    def getData(self):
        return {
            'device': self.device,
            'reading': self.readings
        }

class VibValue(object):
    def __init__(self, device,
                 readings):
        self.device = device
        self.readings = readings

    def getData(self):
        return {
            'device': self.device,
            'reading': self.readings
        }