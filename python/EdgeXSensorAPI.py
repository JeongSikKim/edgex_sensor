# -*- coding: utf8 -*-

import sys
import os
from flask import Flask
from flask import request
import json
import EdgeXRestAPI
import EdgeXDictionary

app = Flask(__name__)
httpHOST = '0.0.0.0'
httpPORT = 49002


# noise api url
# @app.route("/api/v1/devices/{deviceId}/noise", methods=['PUT'])
@app.route("/api/v1/data", methods=['PUT'])
def api_noise():
    print(type(request.data))
    # print("input[" + request.data + "]")
    # print(request.data['stats'])
    # print("input[" + json.dumps(request.data) + "]")
    msg = json.loads(request.data)
    print(type(msg))
    print(msg)
    print(type(msg['stats']))
    print(msg['stats'])
    conv = msg['stats'][0]['sd']
    print(type(conv))
    print(conv)

    noise_value = conv['sound']
    print(type(noise_value))
    print(noise_value)

    send_value = json.dumps(conv)
    print(type(send_value))
    print(send_value[1:18:1])

    # dic=json.loads(conv)
    # print(type(dic))
    # print(len(msg['stats']))

    Service_Info_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST="http://localhost:48080/api/v1/")

    noise_body = EdgeXDictionary.NoiseValue(device='iec-device-hecas-sensor-device',
                                            readings=[{"name": "noise", "value": noise_value}])

    Service_Info_restHelper.post_valueDescriptor(noise_body)

    return request.data


# gas api url
# @app.route("/api/v1/devices/{deviceId}/gas", methods=['PUT'])
@app.route("/api/v1/data", methods=['PUT'])
def api_gas():
    # print(type(request.data))
    msg = json.loads(request.data)
    conv = msg['stats'][0]['gas']
    print(conv)
    gas_value= conv['voc']
    print(gas_value)
    send_value = json.dumps(conv)
    print(send_value)

    Service_Info_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST="http://localhost:48080/api/v1/")

    gas_body = EdgeXDictionary.GasValue(device='iec-device-hecas-sensor-device',
                                        readings=[{"name": "gas", "value": gas_value}])

    Service_Info_restHelper.post_valueDescriptor(gas_body)
    return request.data


# vibration api url
# @app.route("/api/v1/devices/{deviceId}/vib", methods=['PUT'])
@app.route("/api/v1/data", methods=['PUT'])
def api_vib():
    msg = json.loads(request.data)
    conv = msg['stats'][0]['vib']
    vib_value = conv['vibration']

    Service_Info_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST="http://localhost:48080/api/v1/")

    vib_body = EdgeXDictionary.GasValue(device='iec-device-hecas-sensor-device',
                                        readings=[{"name": "vib", "value": vib_value}])

    Service_Info_restHelper.post_valueDescriptor(vib_body)
    return request.data


if __name__ == "__main__":
    # app.debug = True
    app.run(httpHOST, httpPORT, debug=True)
