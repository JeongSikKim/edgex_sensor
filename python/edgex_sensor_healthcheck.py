from flask import Flask
from flask import request
from flask_restful import Resource, Api
from flask_restful import reqparse
import requests
import consulate
import json
# import socket


app = Flask(__name__)
api = Api(app)

CONSUL_NAME = "iec-device-hecas-sensor"


def initConsulate():
    print("Register device-sensor to the localhost Consul")
    consul = consulate.Consul()
    # local_ip = socket.gethostbyname(socket.gethostbyname())
    consul.agent.service.register(name=CONSUL_NAME, address="0.0.0.0", port=5001,
                                  httpcheck="http://0.0.0.0:5001/health", interval="10s")

    #    print("Deregister iot-example from the localhost Consul")
    #    consul.agent.service.deregister("iot-example")

    print("Find device-sensor service by service id from the localhost Consul")
    service = consul.catalog.service(CONSUL_NAME)
    print(service)
    # services = consul.agent.services()
    # print(services)
    # r = res.put('http://localhost:5000/api/v1/helath')
    # printResponse(r)
    # response = request.get('http://127.0.0.1:5000/health')
    # r =response.status_code
    # print(r)

class CreateHealth(Resource):
    def get(self):
        try:
            print(" received!! \n")
            print(request.method)
            print(request.url)
            print(request.json)
            return {}
        except Exception as e:
            return {'error': str(e)}


api.add_resource(CreateHealth, '/health')

if __name__ == '__main__':
    initConsulate()
    app.run(debug=True, host='0.0.0.0', port=int('5001'))
    # app.run(debug=True, httpHOST, httpPORT)
