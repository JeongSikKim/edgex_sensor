#!/bin/sh

SRC_DIR=python

echo "Regist EdgeX"
python $SRC_DIR/EdgeXPostHelper.py >/dev/null 2>&1 &

echo "Install python package"
apt-get install -y python-pip >/dev/null 2>&1 &
pip install flask flask_restful consulate >/dev/null 2>&1 &

echo "Regist Consul"
python $SRC_DIR/edgex_sensor_healthcheck.py >/dev/null 2>&1 &
python $SRC_DIR/EdgeXPost.py >/dev/null 2>&1 &
python $SRC_DIR/EdgeXSensorAPI.py >/dev/null 2>&1 &
