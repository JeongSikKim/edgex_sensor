/*

This example reads from the default PCM device
and writes to standard output for 5 seconds of data.

*/

/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API

#include <alsa/asoundlib.h>
#include <math.h>
#include <curl/curl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>

#define MONITOR_ADDR	"127.0.0.1"
#define MONITOR_PORT	49002

int http_put(CURL *curl, char *url, char *body)
{
	CURLcode res;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
 
	/* Perform the request, res will get the return code */ 
	res = curl_easy_perform(curl);

	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, NULL);

	/* Check for errors */ 
	if(res != CURLE_OK) {
		fprintf(stderr, "http_put() failed\n");
	 	return 0;
	}
	return 1;
}

void rest_send(CURL *curl, float noise) {

	char http_url[200];
	char http_body[2000];

	// struct timeval tv;
	// gettimeofday(&tv,NULL);
	// tv.tv_sec // seconds
	// tv.tv_usec // microseconds

	// Register an Export Client (REST)
 	strcpy(http_url, "http://localhost:49002/api/v1/data");
	//sprintf(http_body, "{\"sensor_id\":\"laptop\",\"timestamp\":\"%ld%03ld\",\"noise\":\"%f\"}\n", tv.tv_sec, tv.tv_usec/1000, noise);

	sprintf(http_body, "{\"sensor\": \"sensor_laptop\", \
				\"stats\": \
				[ \
				{ \
      				\"vib\": \
      				{ \
        			\"vibration_sensor_name\": \"vb-8206sd\", \
				\"vibration\": null \
				}, \
				\"gas\": \
				{\
				\"gas_sensor_name\": \"center-321\", \
				\"gas_fw_ver\": \"1.0.0\", \
				\"carbon_monoxide\": 0.0, \
				\"nitrogen_dioxide\": 0.0, \
				\"ammonia\": 0.0, \
				\"propane\": 0.0, \
				\"iso_butane\": 0.0, \
				\"methane\": 0.0, \
				\"hydrogen\": 0.0, \
				\"ethanol\": 0.0, \
				\"voc_sensor_name\": \"YVOCMK02-BDB49\", \
				\"voc\": 450.0 \
				}, \
				\"sd\": \
				{ \
				\"sound_name\": \"center-321\", \
				\"gas_sound_fw_ver\": \"1.0.0\", \
				\"sound\": %f \
				} \
				} \
				] \
			}\n", noise);

//			sprintf(http_body, "{\"noise\": %f \
//            			}\n", noise);

	if(http_put(curl, http_url, http_body)) { 
		//fprintf(stderr, "Send noise Succsfully\n\n\n");	
	}
}

void udp_send(float noise) {

    int sockfd;
    int clilen;

    struct sockaddr_in serveraddr;

    clilen = sizeof(serveraddr);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket error : ");
        exit(0);
    }

    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = inet_addr(MONITOR_ADDR);
    serveraddr.sin_port = htons(MONITOR_PORT);

    sendto(sockfd, (void *)&noise, sizeof(noise), 0, (struct sockaddr *)&serveraddr, clilen);

    close(sockfd);
}

int main() {

	CURL *curl;
	struct curl_slist *slist=NULL;

	long loops;
	int rc;
	int size;
	snd_pcm_t *handle;
	snd_pcm_hw_params_t *params;
	unsigned int val;
	int dir;
	snd_pcm_uframes_t frames;
	char *buffer;
	double k = 0.45255;

	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);
 
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
		slist = curl_slist_append(slist, "Content-Type: application/json");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	}
	else {
		fprintf(stderr, "curl_easy_init() failed\n");
		curl_global_cleanup();
		return -1;
	}

	/* Open PCM device for recording (capture). */
	rc = snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);
	if (rc < 0) {
		fprintf(stderr,
		"unable to open pcm device: %s\n",
		snd_strerror(rc));
		exit(1);
	}

	/* Allocate a hardware parameters object. */
	snd_pcm_hw_params_alloca(&params);

	/* Fill it in with default values. */
	snd_pcm_hw_params_any(handle, params);

	/* Set the desired hardware parameters. */

	/* Interleaved mode */
	snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);

	/* Signed 16-bit little-endian format */
	snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);

	/* Two channels (stereo) */
	snd_pcm_hw_params_set_channels(handle, params, 2);

	/* 44100 bits/second sampling rate (CD quality) */
	val = 44100;
	snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);

	/* Set period size to 32 frames. */
	frames = 32;
	snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);

	/* Write the parameters to the driver */
	rc = snd_pcm_hw_params(handle, params);
	if (rc < 0) {
		fprintf(stderr,
		"unable to set hw parameters: %s\n",
		snd_strerror(rc));
		exit(1);
	}

	/* Use a buffer large enough to hold one period */
	snd_pcm_hw_params_get_period_size(params, &frames, &dir);
	size = frames * 4; /* 2 bytes/sample, 2 channels */
	buffer = (char *) malloc(size);

	/* We want to loop for 5 seconds */
	snd_pcm_hw_params_get_period_time(params, &val, &dir);
	//loops = 5000000 / val;
	loops = 1000000 / val;

	float noise = 0.0f;
	int hq_stream = 0; 
	int cont = 0;
	int dB = 0;

	//system("sudo ./killvlc.sh");
	//system("./stream.sh 1 &"); 

	while(1) {
	//while (loops > 0) {
		loops--;
		rc = snd_pcm_readi(handle, buffer, frames);
		if (rc == -EPIPE) {
			/* EPIPE means overrun */
			fprintf(stderr, "overrun occurred\n");
			snd_pcm_prepare(handle);
		} else if (rc < 0) {
			fprintf(stderr,
			"error from read: %s\n",
			snd_strerror(rc));
		} else if (rc != (int)frames) {
			fprintf(stderr, "short read, read %d frames\n", rc);
		}
		//rc = write(1, buffer, size);
		//if (rc != size)
		//	fprintf(stderr,
		//		"short write: wrote %d bytes\n", rc);

		short *data;
		float scale;
		float peak = 0.0f;
		long int square_sum = 0.0;

		for(int i=0; i<frames; i++) {
			data = (short *)(buffer+4*i);
			square_sum += (*data * *data);
			data = (short *)(buffer+4*i+2);
			square_sum += (*data * *data);
		}

		double result = sqrt(square_sum/(frames*2));

		dB = (int)20*log10(result * k);
		//dB = 60;


		//printf("peak = %f\n", peak);
		noise = noise*0.9 + dB*0.1;
		//printf("noise = %f\n", noise);
		
		if(loops == 0) {
			FILE *fp = fopen("/tmp/noise", "w");
			if (fp) {
				fprintf(fp, "{\"noise\": %f}\n", noise);
				fclose(fp);
			}
			printf("{\"noise\": %f}\n", noise);
			//udp_send(noise);
			rest_send(curl, noise);
			loops = 1000000 / val;
		}

	}

	snd_pcm_drain(handle);
	snd_pcm_close(handle);
	free(buffer);

	return 0;
}

