#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <curl/curl.h>


int http_put(CURL *curl, char *url, char *body)
{
	CURLcode res;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
 
	/* Perform the request, res will get the return code */ 
	res = curl_easy_perform(curl);

	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, NULL);

	/* Check for errors */ 
	if(res != CURLE_OK) {
		fprintf(stderr, "http_put() failed\n");
	 	return 0;
	}
	return 1;
}

void rest_send(CURL *curl, float noise) {

	char http_url[200];
	char http_body[2000];

	// struct timeval tv;
	// gettimeofday(&tv,NULL);
	// tv.tv_sec // seconds
	// tv.tv_usec // microseconds

	// Register an Export Client (REST)
 	strcpy(http_url, "http://127.0.0.1:49002/api/v1/data");
	//sprintf(http_body, "{\"sensor_id\":\"emulator\",\"timestamp\":\"%ld%03ld\",\"noise\":\"%f\"}\n", tv.tv_sec, tv.tv_usec/1000, noise);

	sprintf(http_body, "{\"sensor\": \"sensor_emulator\", \
				\"stats\": [ \
				{ \
      				\"vib\": { \
        			\"vibration_sensor_name\": \"vb-8206sd\", \
				\"vibration\": null \
				}, \
				\"gas\": {\
				\"gas_sensor_name\": \"center-321\", \
				\"gas_fw_ver\": \"1.0.0\", \
				\"carbon_monoxide\": 0.0, \
				\"nitrogen_dioxide\": 0.0, \
				\"ammonia\": 0.0, \
				\"propane\": 0.0, \
				\"iso_butane\": 0.0, \
				\"methane\": 0.0, \
				\"hydrogen\": 0.0, \
				\"ethanol\": 0.0, \
				\"voc_sensor_name\": \"YVOCMK02-BDB49\", \
				\"voc\": 450.0 \
				}, \
				\"sd\": { \
				\"sound_name\": \"center-321\", \
				\"gas_sound_fw_ver\": \"1.0.0\", \
				\"sound\": %f \
				} \
				} \
				] \
			}\n", noise);

	if(http_put(curl, http_url, http_body)) { 
		//fprintf(stderr, "Send noise Succsfully\n\n\n");	
	}
}

int main(int argc, char *argv[]) {

	CURL *curl;
	struct curl_slist *slist=NULL;

	float sound;
	FILE *fp; 
	char line[512], time[30];
	int interval;


	fp = fopen(argv[1], "r");
	interval = atoi(argv[2]);

	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);
 
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
		slist = curl_slist_append(slist, "Content-Type: application/json");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	}
	else {
		fprintf(stderr, "curl_easy_init() failed\n");
		curl_global_cleanup();
		return -1;
	}

	if(fp != NULL) {
		while(1) {

			memset(line, 0x00, sizeof(line));

			if(fgets(line, sizeof(line), fp)==NULL)
				break;

			char *pch;
			pch = strtok(line,",");
			int i;
			for(i=0 ; i<2 ; i++) {
				//printf("[%s]",pch);
				if(i==0) 
					strcpy(time, pch);
				if(i==1)
					sound = atof(pch);				
				pch = strtok(NULL,",");
			}

			// skip the first line...
			if(!strcmp(time, "Time"))
				continue; 

			printf("%f at %s\n", sound, time);
			rest_send(curl, sound);

			//SLEEP:
			if(interval != 0)
				usleep(interval*1000);

		}
	}

	if(curl)
		curl_easy_cleanup(curl);
	curl_global_cleanup();

	return 0;
}
